/*ESTRUCTURA DEL OBJETO PROFESOR*/
profesor={
  
  "clave": "1010",
  "nombre": "arturo lopez",
  "grupos": [
  {
    "clave": "2","nombre": "fisica","alumnos": [
    {"no_control": "1100","nombre": "ana juarez"},
      {"no_control": "2200","nombre": "maria canceco"}]
    },
    {
      "clave": "3","nombre": "quimica","alumnos": [
    {"no_control": "3300","nombre": "juan perez"},
    {"no_control": "4400","nombre": "mario iba�ez"},
    {"no_control": "5500","nombre": "carlos hernandez"},
    {"no_control": "6600","nombre": "beatriz perez"}]
    }
  ]
  
}

/*FUNCION QUE BUSCA SI EXISTE LA CLAVE DEL GRUPO*/

function numero_alumnos(profesor,llave)
{
  for(var i=0;i<profesor["grupos"].length;i++)
  {
    if(profesor["grupos"][i]["clave"]==llave)
    {
      return numero_alumno(profesor["grupos"][i]);
    }
  }
  return 0;
}

/*FUNCION QUE DEVUELVE EL NUMERO DE ALUMNOS*/

function numero_alumno(profesor)
{
  return profesor["alumnos"].length; 
}

/*LLAMADA A LA FUNCION numero_alumnos*/

console.log(numero_alumnos(profesor,"2"));